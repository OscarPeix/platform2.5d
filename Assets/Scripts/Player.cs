﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Vector2 axis;
    private Rigidbody rb;

    private bool jumping;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        jumping = false;
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        //Movimiento Horizontal
        rb.velocity = new Vector3(axis.x * 10, rb.velocity.y, 0);

        //Movimiento Vertical
        if (!jumping && axis.y > 0.2f)
        {
            jumping = true;
            rb.AddForce(Vector3.up * 8, ForceMode.Impulse);
        }
    }

    public void SetAxis(Vector2 input)
    {
        axis = input;
    }


    private void OnCollisionEnter(Collision collision)
    {
        jumping = false;
    }

}